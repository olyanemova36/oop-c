#include "pch.h"
#include "Multiedge.h"
#include "gtest\gtest.h"
#include <math.h>

TEST(PolygonConstructor, DefaultConstructor)
{
	Geometry::Multyedge Figure;
	ASSERT_EQ(0, Figure.GetCord(1).x);
	ASSERT_EQ(0, Figure.GetCord(1).y);
}

TEST(PolygonConstructor, InitConstructors)
{
	Geometry::Point P(1, 1);
	Geometry::Multyedge  Figure1(P);
	ASSERT_EQ(1, Figure1.GetCord(1).x);
	ASSERT_EQ(1, Figure1.GetCord(1).y);
}

TEST(PolygonConstructor, CopyConstructors)
{
	Geometry::Point P(1, 1);
	Geometry::Multyedge  Figure1(P);
	Geometry::Multyedge CopyFigure(Figure1);
	ASSERT_EQ(1, CopyFigure.GetCord(1).x);
	ASSERT_EQ(1, CopyFigure.GetCord(1).y);
}
TEST(PolygonConstructor, TransporationConstructors)
{
	Geometry::Point P(1, 1);
	Geometry::Multyedge  Figure1(P);
	Geometry::Multyedge MoveFigure = Figure1;
	ASSERT_EQ(1, MoveFigure.GetCord(1).x);
	ASSERT_EQ(1, MoveFigure.GetCord(1).y);

}
TEST(PolygonConstructor, FullConstructors)
{
	Geometry::Point A = { 1, 1 };
	Geometry::Point Ary[] = { {1., 1.}, {2., 2.}, {3., 3.} };
	int Power = 3;
	Geometry::Multyedge TriangleClear(Ary,sizeof(Ary)/sizeof(Ary[0]), Power);

	ASSERT_EQ(2, TriangleClear.GetCord(2).x);
	ASSERT_EQ(2, TriangleClear.GetCord(2).y);
}

TEST(PolygonConstructor, ExceptionConstructors)
{
	Geometry::Point Ary[] = {1, 1, 2, 2, 3, 3, 4, 4};
	int Power = 3;
	ASSERT_ANY_THROW(Geometry::Multyedge Triangle(Ary, sizeof(Ary)/ (2*sizeof(Ary[0])), Power));
}

TEST(PolygonConstructor, ExceptionGetter)
{
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure1(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	ASSERT_ANY_THROW(Figure1.GetCord(4));
	ASSERT_EQ(2, Figure1.GetCord(2).y);
	ASSERT_EQ(2, Figure1.GetCord(2).x);
}

TEST(PolygonMethods, PolugonPushEdge)
{
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Point NewOne = { 4, 4 };
	Geometry::Multyedge  Figure(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Figure.PushEdge(NewOne);
	ASSERT_EQ(4, Figure.GetCord(4).x);
	ASSERT_EQ(1, Figure.GetCord(1).y);
	ASSERT_EQ(1, Figure.GetCord(1).x);
}

TEST(PolygonMethods, PolugonRotation)
{
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge  Figure(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Figure.Rotation(360, { 0,0 });
	ASSERT_EQ(3, Figure.GetCord(3).y);
	ASSERT_EQ(3, Figure.GetCord(3).x);
}
TEST(PolygonMethods, PolugonMoving)
{
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Geometry::Point vector = { 2, 2 };
	Figure.MoveTo(vector);
	ASSERT_EQ(5, Figure.GetCord(3).y);
	ASSERT_EQ(5, Figure.GetCord(3).x);
}

TEST(PolygonMethods, PlygonHardCenter)
{
	Geometry::Point P(1, 1);
	Geometry::Multyedge Figure1(P);
	ASSERT_EQ(P.x, Figure1.HardCenter().x);
	ASSERT_EQ(P.y, Figure1.HardCenter().y);
}
TEST(PolygonOperators, PlygonEqual) {
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure_(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Geometry::Multyedge Figure_new;
	Figure_new = Figure_;
	ASSERT_EQ(Figure_new[2].x, Figure_[2].x);
	ASSERT_EQ(Figure_new[2].y, Figure_[2].y);
}
TEST(PolygonOperators, PlygonMinusMinus) {
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure_(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	--Figure_;
	ASSERT_ANY_THROW(Figure_.GetCord(3));
	Figure_--;
	ASSERT_ANY_THROW(Figure_.GetCord(2));
}
TEST(PolygonOperators, PlygonPlus) {

	Geometry::Point P(1, 1);
	Geometry::Multyedge Figure1(P);

	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure2(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);

	Geometry::Multyedge Figure3;
	Figure3 = Figure1 + Figure1;
	ASSERT_EQ(Figure3.GetPower(), 2);
	ASSERT_EQ(Figure3.GetCord(2).x, 1);
	ASSERT_EQ(Figure3.GetCord(2).y, 1);

	Figure2 += Figure1;

	ASSERT_EQ(Figure2.GetPower(), 4);
	ASSERT_EQ(Figure2.GetCord(4).x, 1);
	ASSERT_EQ(Figure2.GetCord(4).y, 1);
}
TEST (PolygonOperators, PlygonIndex) {
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure_(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Geometry::Point P = { 1, 1 };

	ASSERT_EQ(Figure_[0].x, P.x);
	ASSERT_EQ(Figure_[0].y, P.y);
}
TEST(PolygonOperators, PlygonInputOutput) {
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure_(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	std::cout << Figure_ << std::endl;
}

TEST(PolygonOperators, PlygonUnar) {
	Geometry::Point Ary[] = { {1, 1}, {2, 2}, {3, 3} };
	int Power = 3;
	Geometry::Multyedge Figure_(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);

	Geometry::Multyedge Figure3;
	Figure3 = (-Figure_);

	ASSERT_EQ(Figure3[0].x, -1);
	ASSERT_EQ(Figure3[0].y, -1);
}

TEST(PolygonConstant, PlygonConstructors) {
	Geometry::Multyedge const Figure;
	ASSERT_EQ(0, Figure.GetCord(1).x);
	ASSERT_EQ(0, Figure.GetCord(1).y);

	Geometry::Point P(1, 1);
	Geometry::Multyedge const Figure1(P);
	ASSERT_EQ(1, Figure1.GetCord(1).x);
	ASSERT_EQ(1, Figure1.GetCord(1).y);

	Geometry::Multyedge const CopyFigure(Figure1);
	ASSERT_EQ(1, CopyFigure.GetCord(1).x);
	ASSERT_EQ(1, CopyFigure.GetCord(1).y);

	Geometry::Point A = { 1, 1 };
	Geometry::Point Ary[] = { {1., 1.}, {2., 2.}, {3., 3.} };
	int Power = 3;
	Geometry::Multyedge const TriangleClear(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);

	ASSERT_EQ(2, TriangleClear.GetCord(2).x);
	ASSERT_EQ(2, TriangleClear.GetCord(2).y);
}

TEST(PolygonConstant, PlygonMethods) {

	Geometry::Point P(1, 1);
	Geometry::Multyedge const Figure1(P);
	ASSERT_EQ(P.x, Figure1.HardCenter().x);
	ASSERT_EQ(P.y, Figure1.HardCenter().y);

	ASSERT_EQ(1, Figure1.GetPower());
	ASSERT_EQ(120, Figure1.GetMaxPower());
	Figure1.print_(std::cout);
}
TEST(PolygonConstant, PlygonOperators) {
	Geometry::Point A = { 1, 1 };
	Geometry::Point Ary[] = { {1., 1.}, {2., 2.}, {3., 3.} };
	int Power = 3;
	Geometry::Multyedge const TriangleClear(Ary, sizeof(Ary) / sizeof(Ary[0]), Power);
	Geometry::Point P = { 1, 1 };

	ASSERT_EQ(TriangleClear[0].x, P.x);
	ASSERT_EQ(TriangleClear[0].y, P.y);

	std::cout << TriangleClear;

}
