#pragma once

#ifndef Multiedge_h
#define Multiedge_h

#define _USE_MATH_DEFINES
#include <stdlib.h>
#include <crtdbg.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <stdexcept>

#define PI  3.14159


namespace Geometry {

	struct Point {
		float x;
		float y;
		Point(float x0 = 0, float y0 = 0) :x(x0), y(y0) {};
	};

	class Multyedge {
	private:
		const static int MaxPower = 120;
	    int Power;
		Point *edges;
	public:

		Multyedge(); // Default Constructor
		Multyedge(Point& egde) : Power(1) {
			this->edges = new Point[1];
			this->edges[0] = egde;
		}; // Constructor 1 point
		Multyedge(Point* Ary, int ary_size, int Edges); // Inicialize Constructor

		Multyedge(const Multyedge& Figure) { //Copy constructor
			this->edges = new Point[Figure.Power];
			this->Power = Figure.Power;

			for (int i = 0; i < this->Power; i++) {
				this->edges[i] = Figure.edges[i];
			}
		}
		Multyedge(Multyedge&& Figure) noexcept : edges(Figure.edges)  {
			this->Power = Figure.Power;
			Figure.edges = nullptr;
			Figure.Power = 0;
		}

		~Multyedge() {
			delete[] edges;
		}
		
		//Methods

		int GetPower() const { return this->Power; };
		int GetMaxPower() const { return this->MaxPower; };
		Point HardCenter() const;
		Multyedge& PushEdge(const Point A);
		Point GetCord(int Ord) const;
		Multyedge& Rotation(const float fi, const Point p);
		Multyedge& MoveTo(Point vector);

		std::ostream& print_(std::ostream &flow) const;
		std::istream& input_(std::istream& flow);

		//operators

		Multyedge& operator+= (const Multyedge& Figure) {

			if (!(this->Power + Figure.Power <= this->MaxPower)) {
				throw std::exception("Overloaded!");
			}
			else { 
				Point* temporary = new Point[this->Power];
				for (int i = 0; i < this->Power; i++) {
					temporary[i] = this->edges[i];
				}
				delete[] this->edges;
				this->edges = new Point[this->Power + Figure.Power];
				for (int i = 0; i < this->Power; i++) {
					this->edges[i] = temporary[i];
				}
				for (int i = 0; i < Figure.Power; i++) {
					this->edges[this->Power + i] = Figure.edges[i];
				}
				this->Power = this->Power + Figure.Power;
			}
			return *this;
		};
		

		Multyedge& operator--() {
			if ((this->Power - 1) <= 0) {
				throw std::exception("Invalid difference!");
			}
			else {
				this->Power = this->Power - 1;
				Point* temporary = new Point[this->Power];
				for (int i = 0; i < this->Power; i++) {
					temporary[i] = this->edges[i];
				}
				delete[] this->edges;
				this->edges = new Point[Power];
				for (int i = 0; i < this->Power; i++) {
					this->edges[i] = temporary[i];
				}
				delete[] temporary;
			}
			return *this;
		}
		const Multyedge& operator--(int x) {
			Multyedge Figure = *this;
			if (this->Power - 1 <= 0) {
				throw std::exception("Invalid Figure!");
			}
			else {
				Point* temporary = new Point[this->Power];
				for (int i = 0; i < this->Power; i++) {
					temporary[i] = this->edges[i];
				}
				delete[] this->edges;
				this->Power--;
				this->edges = new Point[this->Power];
				for (int i = 0; i < this->Power; i++) {
					this->edges[i] = temporary[i];
				}
			}
			return Figure;
		}

		Multyedge operator+ (const Multyedge& Figure) const {
			Multyedge One = *this;
			return One += Figure;
		};


		Multyedge& operator- () {
			for (int i = 0; i < this->Power; i++) {
				this->edges[i].x = -this->edges[i].x;
				this->edges[i].y = -this->edges[i].y;
			}
			return *this;
		}
		Multyedge& operator+ () {
			return (-(-*this));
		} 

		Point& operator[] (int i) {
			return this->edges[i];
		}
		const Point& operator[] (int i) const {
			return this->edges[i];
		}

		Multyedge& operator= (const Multyedge& obj_) {
			if (&obj_ == this) {
				return *this;
			}
			this->Power = obj_.Power;
			delete[] this->edges;
			this->edges = new Point[obj_.Power];

			for (int i = 0; i < obj_.Power; i++) {
				this->edges[i] = obj_.edges[i];
			}
			return *(this);
		}
		Multyedge& operator= (Multyedge&& obj_) noexcept {
			if (this != &obj_)
			{
				delete[] this->edges;
				this->Power = obj_.Power;
				this->edges = obj_.edges;
				obj_.edges = nullptr;
			}
			
			return *this;
		}
		//friends

		friend std::ostream& operator<< (std::ostream& out, const Multyedge& C_);
		friend std::istream& operator>> (std::istream& in, Multyedge& C_);

	};
	
}


#endif /* Multiedge_h  */ 
