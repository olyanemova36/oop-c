#include <strstream>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <crtdbg.h>
#include "Multiedge.h"
#include <math.h>

// debug_new.cpp
// compile by using: cl /EHsc /W4 /D_DEBUG /MDd debug_new.cpp
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>

#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#include <iostream>
#include "Multiedge.h"

namespace Geometry {

	Multyedge::Multyedge(Point* Ary, int ary_size, int Edges) : Power(Edges) {

		if (Edges != ary_size) {
			throw std::exception("Illegal source data!");
		}
		if (Edges > this->MaxPower)
			throw std::exception("illegal source data!");
		this->edges = new Point[Edges];
		for (int i = 0; i < Edges; i++) {
			this->edges[i] = Ary[i];
		}
	};

	Multyedge::Multyedge() : Power(1) {
		this->edges = DBG_NEW Point[1];
		this->edges[0].x = 0;
		this->edges[0].y = 0;
	};

	Point Multyedge::HardCenter() const {
		if (this->Power == 1) {
			return this->edges[0];
		}
		Point G;
		float A = 0;
		G.x = 0, G.y = 0;
		for (int i = 0; i < this->Power; i++) {
			G.x += (this->edges[i].x + this->edges[i + 1].x) * (this->edges[i].x * this->edges[i + 1].y - this->edges[i + 1].x * this->edges[i].y);
			G.y += (this->edges[i].y + this->edges[i + 1].y) * (this->edges[i].x * this->edges[i + 1].y - this->edges[i + 1].x * this->edges[i].y);
			A += (this->edges[i].x * this->edges[i + 1].y - this->edges[i + 1].x * this->edges[i].y);
		}
		if (A == 0.0)
			throw std::exception("illgal source data!");
		else {
			A = A / 2;
			G.x = G.x / (6 * A);
			G.y = G.y / (6 * A);

		}
		return G;
	};
	Multyedge& Multyedge::PushEdge(Point A) {
		if (this->Power == this->MaxPower)
			throw std::exception("Overloaded");
		this->Power++;
		Point *temporary = DBG_NEW Point[this->Power];
		for (int i = 0; i < this->Power; i++) {
			temporary[i] = this->edges[i];
		}
		delete[] this->edges;
		this->edges = DBG_NEW Point[this->Power];
		for (int i = 0; i < (this->Power-1); i++) {
			this->edges[i] = temporary[i];
		}
		this->edges[Power-1] = A;
		delete[] temporary;
		return *this;
	};
	Point Multyedge::GetCord(int Ord) const {
		if ((Ord <= 0) || (Ord > this->Power)) 
			throw std::exception("illegal orded");
		else 
			return this->edges[Ord-1];
	};
	Multyedge& Multyedge::Rotation(float fi, const Point P) {
		if (fi == 360) {
			return *this;
		}
		const float g2r = PI / 180.;
		fi *= g2r; // Radians

		Point Pivot{ 0, 0 };
		for (int i = 0; i < this->Power; i++) {
			this->edges[i].x -= Pivot.x;
			this->edges[i].y -= Pivot.y;

			float source_x = this->edges[i].x;
			this->edges[i].x = (this->edges[i].x -P.x)* cosf(fi) - (this->edges[i].y -P.y) * sinf(fi) + P.x;
			this->edges[i].y = (source_x - P.x )* sinf(fi) + (this->edges[i].y -P.y )* cosf(fi) + P.y;
		}
		return *this;
	};

	Multyedge& Multyedge::MoveTo(Point vector) {

		for (int i = 0; i < this->Power; i++) {
			this->edges[i].x += vector.x;
			this->edges[i].y += vector.y;
		}
		return *this;
	};

	std::ostream& Multyedge::print_(std::ostream& flow) const {
		if (this->Power == 0) {
			return flow;
		}
		for (int i = 0; i < this->Power; i++) {
			flow << "{" << this->edges[i].x << ", " << this->edges[i].y << "}" << std::endl;
		}
		return flow;
	};

	std::istream& Multyedge::input_(std::istream& flow) {
		std::cout << "Enter power and data!" << std::endl;
		flow >> this->Power;
		if (this->MaxPower == 0) {
			return flow;
		}
		this->edges = DBG_NEW Point[this->Power];
		for (int i = 0; i < this->Power; i++) {
			flow >> this->edges[i].x;
			flow >> this->edges[i].y;
		}
		return flow;
	}

	//operators
	std::ostream& operator<< (std::ostream& out, const Multyedge& C_) {

		for (int i = 0; i < C_.GetPower(); i++) {
			out << "{" << C_.edges[i].x << ", " << C_.edges[i].y << "}" << std::endl;
		}
		return out;
	}
	std::istream& operator>> (std::istream& in, Multyedge& C_) {

		std::cout << "Enter power and data!" << std::endl;
		in >> C_.Power;
		C_.edges = DBG_NEW Point[C_.Power];
		for (int i = 0; i < C_.Power; i++) {
			in >> C_.edges[i].x;
			in >> C_.edges[i].y;
		}
		return in;
	}
}
