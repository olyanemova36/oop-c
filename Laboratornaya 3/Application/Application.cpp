// debug_new.cpp
// compile by using: cl /EHsc /W4 /D_DEBUG /MDd debug_new.cpp
#define _CRTDBG_MAP_ALLOC
#include <cstdlib>
#include <crtdbg.h>

#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#include <iostream>
#include "Multiedge.h"

template <class Get>
int getNum(Get&);
int dialog(const char* msgs[], int N);


int main(int argc, const char* argv[])

{const char* msgs[] = { "0.Quit",
						   "1.Create a polygon",
	                       "2.Minus 1 Point from Figure",
						   "3.Find a Hard Center",
						   "4.Push new Edge",
						   "5.Get edge's coordinates ",
						   "6.Rotate polygon on angle ",
						   "7.Parallel transporation to vector",
	                       "8.Show",
	                       "9.Plus Squere",
						   "10.Inversion polygon",
						   "11.Positive Inversion polygon",
	};
    int* a = DBG_NEW int;
	const int NMsgs = sizeof(msgs) / sizeof(msgs[0]);
	int current = 0, state = 0;

	Geometry::Multyedge Triangle;

	do {
		state = dialog(msgs, NMsgs);

		if (state == 0) {
			Triangle.~Multyedge();
			exit(1);
		}
		else if (state == 1) {
			int state1;
			Geometry::Multyedge _Figure; // default 
			Triangle = _Figure;
			std::cout << _Figure;
		}
		if (state == 2) {
			try {
				--Triangle;
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}
		}
		if (state == 3) 
			std::cout << "BariCenter is: " << "{ " << Triangle.HardCenter().x << ", " << Triangle.HardCenter().y << " }" << std::endl;
		else if (state == 4) {
			std::cout << "Enter coordinates of new Edge: " << std::endl;
			Geometry::Point One;
			getNum(One.x);
			getNum(One.y);
			try {
				Triangle.PushEdge(One);
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}
		}
		else if (state == 5) {
			std::cout << "Enter Order of Edge: " << std::endl;
			int Ord;
			getNum(Ord);
			try {
				Triangle.GetCord(Ord);
				std::cout << Triangle[Ord-1].x << Triangle[Ord - 1].y << std::endl;
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}
		} 
		else if (state == 6) {
			std::cout << "Enter value of angle in degrees: " << std::endl;
			float Fi;
			getNum(Fi);
			std::cout << "Enter the rotation point: " << std::endl;
			Geometry::Point p;
			getNum(p.x);
			getNum(p.y);
			Triangle.Rotation(Fi, p);
		}
		else if (state == 7) {
			std::cout << "Enter value of vector for parallel transporation: " << std::endl;
			Geometry::Point vector;
			getNum(vector.x);
			getNum(vector.y);
			Triangle.MoveTo(vector);
		}
		else if (state == 8) {
			std::cout << Triangle;
		}
		else if (state == 9) {
			Geometry::Multyedge Squere;
			std::cout << "Enter data for Squere with number of Power is 4";
			std::cin >> Squere;
			Triangle += Squere;
			std::cout << Triangle;
		}
		else if (state == 10) {
			Triangle = (-Triangle);
		}
		else if (state == 11) {
			Triangle = (+Triangle);
		}
	} while (state != 0);

	std::cout <<"That's all. Bye!" << std::endl;
	_CrtDumpMemoryLeaks();
	return 0;
}

int dialog(const char* msgs[], int N) {
	const char* pr = "";
	int choise;
	int state;

	do {

		std::cout << pr << std::endl;
		pr = "You are wrong; repeat please!";

		for (int i = 0; i < N; ++i) {
			puts(msgs[i]);
		}

		puts("Make your choise -->");
		state = getNum(choise);

	} while (state < 0 || state > N);

	return choise;
}

template <class Get>
int getNum(Get& a) {
	std::cin >> a;
	if (!std::cin.good()) {
		return 0;
	}
	return 1;
}
