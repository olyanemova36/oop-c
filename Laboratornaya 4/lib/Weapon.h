//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_WEAPON_H
#define SECURITYSYSTEM_WEAPON_H
#include "Site.h"
#include "Module.h"
#include "Violator.h"
namespace Security {
    /**
     * \brief The Heir class for realisating the Platform interface in killing terms.
     * \details The class describes the abstract Module class. The one of Module types for performing the killings in current location of the module.
     * \date 22.12.2021
     * \author Nemova Olga
     */
    class Weapon: public Module {
    private:
        bool isCharging;
    public:
        /**
         * The default constructor for Weapon. Set the default characteristics like range of action and ets...
         */
        Weapon();
        /**
         * The Inicialized constructor, which creates the turn on or turn down Weapon modules. If the Module is turn down it is inactive.
         * @param turnOn : The state on turning on of Network module. If it is turn down you can't use it.
         */
        explicit Weapon(bool turnOn);
        /**
         * Setter for assigning the charging state. You can set the charging state when the module's energy supply in 0 point.
         * @param state : The new state of charging position. If module is charging it is not available for usage.
         */
        void setChargingState (bool state);
        /**
         * The public method for set the module on charge. It uses the Weapon Setter and checks the the level of Energy supply.
         * @param state : The new state of charging position. If module is charging it is not available for usage.
         */
        void Charging(bool state);
        /**
         * The method for killing the Violators. It delete in in current location and Security System can't see it there anymore.
         * @param person : The pointer on which person is needs for deleting from current location.
         * @param apart  : The location in which person is needs for deleting from current location.
         */
        void killViolator( Violator &person, NewSite* apart) const;
        /**
         * Getter for returning the charging state.
         * @return The state of charging position. If module is charging it is not available for usage.
         */
        bool getChargeState() const;
        /**
         * Method for specifiing the type of Module is it.
         * @return The type of Module is it.
         */
        virtual Type whoIAm() const;
    };
}

#endif //SECURITYSYSTEM_WEAPON_H
