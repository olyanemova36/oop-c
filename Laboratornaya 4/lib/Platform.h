//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_PLATFORM_H
#define SECURITYSYSTEM_PLATFORM_H
#include <string>
#include <vector>
#include "Cell.h"
#include "Module.h"
#include "NewSite.h"

namespace Security {
    /**
     * \brief The Base class for creating the constructing counts of Sycurity System.
     * \details The class for describing the main consist System Security elements for managing the Security in some location.
     * \date 22.12.2021
     * \author Nemova Olga
     *
     * */

    class Platform: public Cell {
    protected:
        NewSite* room;
        std::string description;
        int energySupply; // Энергопообеспечение
        int slots;
        int slots_current;
        std::map<int, Module&> modules;
    public:
        /**
         * Default constructor. Set the default quantites on all field of class.
         */
        Platform();
        /**
         * Inicilized constructor. Creates the platform from some location in the site. Assign it on this coordinates.
         * @param cell : The cell on which parameters will be created the new stationary Platform.
         */
        explicit Platform(const Cell& cell);
        /**
         * Inicilized constructor. Creates the platform from some location in the site. Assign it on this coordinates. And also assign the location of this Platform due to turn on the Sensor module on this platform.
         * @param cell : The cell on which parameters will be created the new stationary Platform.
         * @param room : The location in which the Platform will be situated.
         */
        explicit Platform(const Cell& cell, NewSite* room);

        /**
         * Getter. Returns the current count of slots for modules in this platform. How many modules already there is.
         * @return The count of added modules on this Platform.
         */
        int getCurrentSlots() const;
        /**
         * Getter. Returns the room location where the Platform is located now.
         * @return The NewSite plan where the Platform is located.
         */
        NewSite* getRoom();
        /**
         * Getter. Returns the specialized by type i module from all assigned modules in this platform.
         * @param type : The type of required module.
         * @param i : Which module is required of this type.
         * @return Pointer on the module in the memory.
         */
        Module* getModule(Type type, int i);

        /**
         * Setter for specifing the new room for Platfrom where it can acting.
         * @param room : The new room for assign.
         */
        void setRoom(NewSite* room);
        /**
        * Setter for specifing the new description for platform, which helps the identify the platforms beyond all platforms in room.
        * @param description : String for describing of Dynamic Platform.
        */
        virtual void setDescription(std::string description);
        /**
         * Method adds the new module in Platform unless it capacity doesnt equal current slots in the Platform.
         * @param module : The required module for pushing.
         * @return True if module was saccess added, false in otherwise.
         */
        bool pushModule(Module& module);
        /**
         * Method erase the module by it index in all Modules in the Platform.
         * @param index : The number of module do you want to erase.
         * @return True if module was saccess erase, false in otherwise.
         */
        bool popModule(int index);
        /**
         * Method for specifiing the type of Dynamic Platform is it.
         * @return The type of Platform is it.
         */
        virtual Type whoIAm() const;
    };

}


#endif //SECURITYSYSTEM_PLATFORM_H
