//
// Created by Ольга Немова on 18.12.2021.
//

#include "NewSite.h"

Security::NewSite::NewSite() {
    this->scale = 10;

}

Security::NewSite::NewSite(int _scale) {
    this->scale = _scale;
}

Security::NewSite::NewSite(const Security::NewSite &plan) {
    this->scale = plan.scale;

    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        this->room.erase(it);
    }
    Map<std::pair<int,int>, Cell*>::Iterator it2;
    for (it2  = plan.room.begin(); it2 != plan.room.end(); it2++) {
        this->room.insert(std::pair<std::pair<int, int>, Cell*>{it2->first, it2->second});
    }
}

void Security::NewSite::setNewCoordinates(const std::pair<int, int> &point, int X, int Y) {
    Condition type = this->findCell(point);
    if ((type == VIOLATOR) || (type == BARRIER) || (type == ENGAGED) ) {
        Map<std::pair<int,int>, Cell*>::Iterator it;
        std::cout << "I'm here!" << std::endl;
        for (it = this->room.begin(); it != this->room.end(); it++ ) { //OK
            if (it->first == point) {
                std::cout << "I'm here!" << std::endl;
                if ( this->findCell(std::pair(X, Y)) != EMPTY) {
                    std::cout << "Can't be moved!" << std::endl;
                    return;
                }
                std::cout << "I'm here!" << std::endl;
                Cell *what = it->second;
                what->setCoordinates(X, Y);
                this->room.erase(point);
                this->room.insert(std::pair<std::pair<int, int>, Cell*>(std::pair<int, int>(X, Y), what));
                std::cout << "Go ahead!" << std::endl;
                return;
            }
        }
    }
    else {
        std::cout << "Can't be moved!" << std::endl;
    }
}

Security::Condition Security::NewSite::findCell(const std::pair<int, int> &cell) const {
    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        //std::cout << "I find: " << it->first.first <<" : " << it->first.second  << std::endl;
        if ((it->first.first == cell.first) && (it->first.second == cell.second)) {
            return it->second->getCondition();
        }
    }
    return EMPTY;
}

Security::Condition Security::NewSite::findCell(Security::Condition violator) const {
    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        if (it->second->getCondition() == violator){
            return violator;
        }
    }
    return EMPTY;
}

const Security::Cell *Security::NewSite::getValue(const Security::Cell &cell) const {
    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        if (it->first.first == cell.getX() && it->first.second == cell.getY()) {
            return it->second;
        }
    }
    return nullptr;
}

Security::Cell* Security::NewSite::getCell(const Security::Cell &cell) const {
    Map<std::pair<int, int>, Cell*>::Iterator p = this->room.find(std::pair<int, int>(cell.getX(), cell.getY()));
    if (p == this->room.end()) {
        return nullptr;
    }
    return p->second;
}

void Security::NewSite::pushCell(Security::Cell *what) {
    //std::cout << "OK" << std::endl;
    if (this->room.size() < this->scale) {
        //std::cout << "OK_check_size" << std::endl;
        if (what->getCondition() == EMPTY) {
            //std::cout << "OK_check_condition" << std::endl;
            //std::cout << "The is not supported!" << std::endl;
            return;
        }
        //std::cout << "OK_good_condition" << std::endl;
        if (this->getCell(*what) != nullptr) {
            //std::cout << "OK_check_ptr" << std::endl;
            //std::cout << "This cell already here!" << std::endl;
            return;
        }
        //std::cout << "OK_yes_ptr" << std::endl;
        this->room.insert(std::pair<std::pair<int, int>, Cell*>({what->getX(), what->getY()}, what));
        return;
    }
    std::cout << "The room is overloaded!" << std::endl;

}

void Security::NewSite::deleteCell(int X, int Y, Security::Condition type) {
    if (this->findCell(std::pair<int, int>(X, Y))>=0) {
        Map<std::pair<int,int>, Cell*>::Iterator it;
        std::cout << "I find the violator in this room" << std::endl;
        this->room.erase(std::pair<int, int>(X, Y));
        std::cout << "I killed" << std::endl;
    }
    else {
        //std::cout << "this cell isn't exist!" << std::endl;
    }
}

std::pair< int, int> Security::NewSite::findLong() const {
    std::pair<int, int> max2 = {0, 0};
    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        if (it->first > max2) {
            max2 = it->first;
        }
    }
    return max2;
}

void Security::NewSite::deleteCell(int X, int Y) {
    Map<std::pair<int,int>, Cell*>::Iterator it;
    it = this->room.find(std::pair<int, int>(X, Y));
    //std::cout << "tI will delete" << it->first.first <<" : " << it->first.second  << std::endl;
    this->room.erase(it);
}

Security::NewSite &Security::NewSite::Restructure(int scale_) {
    if (scale_ > this->scale) {
        return *this;
    }
    while (this->scale != scale_) {
        std::pair<int, int> max = this->findLong();
        std::cout << max.first <<" : " << max.second <<std::endl;
        this->deleteCell(max.first, max.second);
        scale_+=1;
    };
    return *this;
}

Security::NewSite &Security::NewSite::operator=(const Security::NewSite &location) {
    if (&location == this) {
        return *this;
    }
    this->scale = location.scale;

    Map <std::pair<int, int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        this->room.erase(it);
    }
    Map <std::pair<int, int>, Cell*>::Iterator it1;
    for (it1 = location.room.begin(); it1 !=  location.room.end(); it1++) {
        this->room.insert({it1->first, it1->second});
    }
    return *this;
}
void Security::NewSite::print(int scale) {
    char violator = 'V';
    std::string head = "There is your room!";
    char platform = 'S';
    char robot = 'R';
    char barrier = '#';
    char field = '+';
    int x = 0 ;
    int y = 0;
    std::pair<int, int> max = this->findLong();
    //heading
    int delim = (15 - head.size())/2;
    for ( int i = 0 ; i < delim; i++) {
        std::cout << '=';
    }
    std::cout << head;
    for ( int i = 0 ; i < delim; i++) {
        std::cout << '=';
    }
    std::cout << std::endl;
    //Рамка (верх и низ)
    std::cout << '+';
    for ( int i = 0 ; i < scale+4; i++) {
        std::cout << '-';
    }
    std::cout << '+' << std::endl;

    if (scale > max.first && scale > max.second) {
        for (y=scale; y > 0; y--) {
            std::cout << "|  ";
            for (x = 1; x <= scale; x++) {
                Condition what = this->findCell(std::pair<int, int>(x, y));
                if (what >= 0) {
                    switch (what) {
                        case ENGAGED:
                            if (this->room.find(std::pair<int, int>(x, y))->second->whoIAm()== DYNAMIC) {
                                std::cout << robot;
                            }
                            else {
                                std::cout << platform;
                            }
                            break;
                        case BARRIER:
                            std::cout << barrier;
                            break;
                        case VIOLATOR:
                            std::cout << violator;
                            break;
                    }
                }else {
                    std::cout << field;
                }
            }
            std::cout << "  |";
            std::cout << std::endl;
        }
        std::cout << '+';
        for ( int i = 0 ; i < scale+4; i++) {
            std::cout << '-';
        }
        std::cout << '+';
    }
    else {
        std::cout << "I can't print this field" << std::endl;
}


};

Security::Cell *Security::NewSite::getCell(Security::Type type) const {
    Map<std::pair<int,int>, Cell*>::Iterator it;
    for (it = this->room.begin(); it != this->room.end(); it++) {
        if (it->second->whoIAm() == type) {
            return it->second;
        }
    }
    return nullptr;
}
