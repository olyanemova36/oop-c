//
// Created by Ольга Немова on 27.11.2021.
//

#include "Cell.h"
namespace Security {

    /**
     * The overloaded operator for comparing objects, less.
     * @param c_  : Constant reference on Comparable object.
     * @return For object type, true if left object is less then comparable, false otherwise.
     */
    bool Cell::operator<(const Cell &c_) const {
        if (this->x < c_.x || this->y < c_.y) {
            return true;
        }
        return false;
    }
    /**
     * The overloaded operator for comparing objects, greater.
     * @param c_  : Constant reference on Comparable object.
     * @return For object type, true if left object is greater then comparable, false otherwise.
     */
    bool Cell::operator>(const Cell &c_) const {
        return c_ < *this;
    }
    /**
     * The overloaded operator for comparing objects.
     * @param c_  : constant reference on Comparable object.
     * @return For object type, true if left object is equal for comparable, false otherwise.
     */
    bool Cell::operator==(const Cell &c_) const {
        if ((this->x == c_.x) && (this->y == c_.y)) {
            return true;
        }
        return false;
    };
    /**
     * Method for object for performing actions through the field.
     * @param where  : The new place (Coordinates) where the object need to allocated.
     */
    void Cell::moveOn(Cell where) {
        this->setCoordinates(where.x, where.y);
    }

    /**
     * Method for identification for applied object with. Help to understand which type of field element is it.
     * @return Enum type of object is it.
     */
    Type Cell::whoIAm() const {
        return NONAME;
    }
    /**
     * Constant overloaded operation for comparing the elements, like greater.
     * @param c_ : reference on Comparable object.
     * @return For object type, true if left object is greater then comparable, false otherwise.
     */
    bool Cell::operator>(Cell &c_) const {
        if (this->x < c_.x || this->y < c_.y) {
            return true;
        }
        return false;
    }
    /**
     * Constant overloaded operation for comparing the elements, like less.
     * @param c_ : reference on Comparable object.
     * @return For object type, true if left object is less then comparable, false otherwise.
     */
    bool Cell::operator>(const Cell &c_) {
        if (this->x < c_.x || this->y < c_.y) {
            return true;
        }
        return false;
    }
}
