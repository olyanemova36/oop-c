//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_SENSOR_H
#define SECURITYSYSTEM_SENSOR_H
#include "Addition.h"
#include "Module.h"
#include "Cell.h"

namespace Security {
    /**
     * \brief The Heir class for realisating the Platform interface in searching the information terms.
     * \details The class describes the abstract Module class. The one of Module types for getting the near information about location in terms of range of action this module.
     * \date 22.12.2021
     * \author Nemova Olga
     *
     * */
    class Sensor: public Module {
    private:
        Shape action;
    public:
        /**
         * The default constructor. Creates the new module with default parameters. It turns on and has te range of action as 3 cells in location.
         */
        Sensor();
        /**
         * The Inicialized constructor, which creates the turn on or turn down Sensor modules. If the Module is turn down it is inactive.
         * @param turnOn : The new state of charging position. If module is charging it is not available for usage.
         */
        explicit Sensor(bool turnOn);
        /**
         * The Inicialized constructor, which creates the turn on or turn down Sensor modules. If the Module is turn down it is inactive. The assign the type of searching the information about the location for different types of availability of different elements.
         * @param turnOn : The new state of charging position. If module is charging it is not available for usage.
         * @param what : The type of Sensor is it. For describing the type of getting the information.
         */
        Sensor(bool turnOn, Shape what);
        /**
         * Method for specifiing the type of Module is it.
         * @return The type of Module is it.
         */
        virtual Type whoIAm() const;
        /**
         * Method for getting the near information about the location. It returns the vector of all main persons, platforms and barriers in terms of range of action this module.
         * @param room : The location what should be checked.
         * @param where : The source point of beginning the searching the information.
         * @return The list of near cells from parameter's location.
         */
        std::vector<Cell> getInformation(NewSite* room, const Cell& where);
    };


}




#endif //SECURITYSYSTEM_SENSOR_H
