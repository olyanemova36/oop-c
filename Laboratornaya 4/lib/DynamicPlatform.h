//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_DYNAMICPLATFORM_H
#define SECURITYSYSTEM_DYNAMICPLATFORM_H
#include "Platform.h"
#include "Site.h"
#include "Cell.h"
#include "Addition.h"

namespace Security {
    /**
     * \brief The Platform heir class with ability to transposition.
     * \details Dynamic Platform class for creating the Security objects like Online stations for managing the Security in room. The derived class from Platform.
     * \date 22.12.2021
     * \author Nemova Olga
     *      */
class DynamicPlatform: public Security::Platform{
    private:
        double speed;
    public:
        /**
         * Default constructor for Dynamic Platform object. Sets the default values, like speed = 1 and null pointer on apartment where this platform is located.
         */
        DynamicPlatform():Security::Platform(){ this->speed = 1; this->room = nullptr;};
        /**
         * Inisialized constructor for Dynamic Platform. Creates the new Dynamic Platform for specified coordinates, Type of engaging and reference on the room where the platform will be act anf locate.
         * @param x0 : The first coordinate on field in Algebraic Coordinate system.
         * @param y0 : The second coordinate on field in Algebraic Coordinate system.
         * @param newType : The type of creating Cell.
         * @param room : Pointer on room where Platform is located.
         */
        explicit DynamicPlatform(int x0, int y0, Condition newType, NewSite *room) : Security::Platform() { this->speed = 1; this->room = room;};
        /**
         * Inisialized constructor for Dynamic Platform. Creates the new Dynamic Platform for specified Cell, Type of engaging and reference on the room where the platform will be act anf locate.
         * @param cell : Reference on Cell in which terms create the new Dynamic Platform (Template object).
         * @param room : Pointer on room where Platform is located.
         */
        explicit DynamicPlatform(const Cell &cell, NewSite* room) : Security::Platform(cell) { this->speed = 1;  this->room = room;};
        /**
         * Inisialized constructor for Dynamic Platform. Creates the new Dynamic Platform for specified Cell, with specified speed and reference on the room where the platform will be act anf locate.
         * @param cell : Reference on Cell in which terms create the new Dynamic Platform (Template object).
         * @param _velocity : Quantity of speed for moving in engaging room.
         * @param room : Pointer on room where Dynamic Platform is located.
         */
        DynamicPlatform(const Cell &cell, double _velocity, NewSite* room) : Security::Platform(cell), speed(_velocity) {this->room = room;};
        /**
         *Method for Dynamic Platform for performing actions through the field. Moving at the coordinates in the location which describes by parameter.
         * @param where : The new place (Coordinates) where the Dynamic Platform need to allocated.
         */
        virtual void moveOn(Cell where);
        /**
         * Method for specifiing the type of Dynamic Platform is it.
         * @return The type of Platform is it.
         */
        [[nodiscard]] virtual Type whoIAm() const;
        /**
         * Setter for specifing the new description for platform, which helps the identify the platforms beyond all platforms in room.
         * @param description : String for describing of Dynamic Platform.
         */
        virtual void setDescription(std::string description);
    };

}





#endif //SECURITYSYSTEM_DYNAMICPLATFORM_H
