//
// Created by Ольга Немова on 30.11.2021.
//

#ifndef SECURITYSYSTEM_SECURITYSYSTEM_H
#define SECURITYSYSTEM_SECURITYSYSTEM_H
#include <vector>
#include "Platform.h"
#include "NetworkModule.h"
#include "Site.h"

namespace Security {
    /**
     * \brief The Main class is assigned on realization the whole program acting.
     * \details The class constructed for performing the Security System acting. It manage the room subjects and use the weapon for deleting the violators (strange persons).
     * \date 22.12.2021
     * \author Nemova Olga
     *
     * */
    class SecuritySystem {
    private:
        Security::NewSite* apartment;
        std::vector<Security::Platform*> stations;
    public:
        /**
         * The Inicilized constructor. Creates the new Security System subject from room, which can manage this room.
         * @param room : The room in which the new system is settled.
         */
        explicit SecuritySystem(Security::NewSite* room);
        /**
         * The method for performing the killing of all violators in the whore available range of action of all settled stations and robots net, which contains the system.
         */
        void killAllBadBoys();
        /**
         * It add the new station in location and expand the range of action of all system as whole.
         * @param platform : The pointer at platform, which is needs to add in system (Only the stationary station).
         */
        void pushStation(Platform *platform);
        friend NetworkModule;
    };

}



#endif //SECURITYSYSTEM_SECURITYSYSTEM_H
