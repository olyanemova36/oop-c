//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_VIOLATOR_H
#define SECURITYSYSTEM_VIOLATOR_H
#include "Cell.h"
#include "NewSite.h"

namespace Security {
    /**
     * \brief The Heir class for realisating the Cell difference in personality terms.
     * \details The class describes the Person violator which can moved through the location and it should be killed by Security System.
     * \date 22.12.2021
     * \author Nemova Olga
     * */
    class Violator: public Cell  {
    protected:
        NewSite* room;
        State motion;
        std::pair<int, int> lastCell;
    public:

        /**
         * The inicilized constructor. Creates the new object example form room pointer and cell, describing its position in the location.
         * @param room : The location where the Violator is acting.
         * @param cell : The point which describing its position in the location.
         */
        explicit Violator(NewSite *room, const Cell& cell);
        /**
         * Algoritmhs which returns the next coordinates for Violator acting in terms of State of motion of this Violator.
         * @return : Element in field which describes the new position of the Violator.
         */
        [[nodiscard]] Cell getNextPosition() const;
        /**
         * Method for Violator for performing actions through the field. Moving at the coordinates in the location which describes by parameter.
         * @param where : The new point for allocation in the current field.
         */
        void moveOn(Cell where) override;
        /**
         * Method for specifiing the Violator is it.
         * @return The Violator is it.
         */
        [[nodiscard]] virtual Type whoIAm() const;
    };
}



#endif //SECURITYSYSTEM_VIOLATOR_H
