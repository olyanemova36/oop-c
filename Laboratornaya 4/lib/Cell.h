//
// Created by Ольга Немова on 27.11.2021.
//

#ifndef SECURITYSYSTEM_CELL_H
#define SECURITYSYSTEM_CELL_H
#include "Addition.h"
namespace Security {
    /**
     * \brief The minimal class for storage the element location and information.
     * \details Base class for definition the place of element in Map. Describe the location and fact of engaging of position in field terms.
     * \date 22.12.2021
     * \author Nemova Olga
     *
     */
    class Cell {
    protected:
        int x;
        Condition type;
        int y;
    public:
        /**
         * Default constructor for Cell type. Set the {0, 0} coordinates for default object and type - EMPTY.
         */
        Cell(): x(0), y(0), type(EMPTY){};
        /**
         * Iniciliazed constructor for Cell type. Set the {x0, y0} coordinates for object and default type - EMPTY.
         * @param x0 : The first coordinate on field in Algebraic Coordinate system.
         * @param y0 : The second coordinate on field in Algebraic Coordinate system.
         */
        Cell(int x0, int y0): x(x0), y(y0), type(EMPTY){};
        /**
         * Iniciliazed constructor for Cell type. Set the {x0, y0} coordinates for object and type - newType.
         * @param x0 : The first coordinate on field in Algebraic Coordinate system.
         * @param y0 : The second coordinate on field in Algebraic Coordinate system.
         * @param newType : The type of creating Cell.
         */
        Cell(int x0, int y0, Condition newType): x(x0), y(y0), type(newType){};
        /**
         * Copy constructor for Cell class. Set the analogous parameters of reference object in new creating object.
         * @param pCell : Referens on template object for new creating the object.
         */
        Cell(const Cell& pCell) {
            this->x = pCell.x;
            this->y = pCell.y;
            this->type = pCell.type;
        }
        /**
         * Setter for engaging the new type of location. Set the new type of object will be there.
         * @param _type : Enum type of engaging the field in map.
         */
        virtual void setCondition(Condition _type) noexcept {
            this->type = _type;
        }
        /**
         * Setter for new coordinates of object. Set the new coordinates for object. Locate it in the new field.
         * @param X : The new first coordinate on field in Algebraic Coordinate system.
         * @param Y : The new second coordinate on field in Algebraic Coordinate system.
         */
        virtual void setCoordinates(int X, int Y) {
            this->x = X;
            this->y = Y;
        }
        /**
         * Getter for returning the object field type. It returns the type of engaging the field. Helps us to figure out what the object is it.
         * @return : Type of Cell. Specified the object.
         */
        Condition getCondition() const {return this->type;}
        virtual int getX () const {return this->x;}
        virtual int getY () const { return this->y;}

        virtual void moveOn(Cell where);
        virtual Type whoIAm() const;

        bool operator<(const Cell& c_) const;
        bool operator>(const Cell& c_) const;
        bool operator>(Cell& c_) const;
        bool operator==(const Cell& c_) const;
    };
}



#endif //SECURITYSYSTEM_CELL_H
