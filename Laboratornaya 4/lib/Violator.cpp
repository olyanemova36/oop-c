//
// Created by Ольга Немова on 24.11.2021.
//

#include "Violator.h"
#include "Cell.h"
#include <cstdlib>
namespace Security {

    Violator::Violator(NewSite *room, const Cell& cell) : Security::Cell(cell), motion(RANDOM), lastCell(0, 0) {
        this->room = room;
    };

    Cell Violator::getNextPosition() const {
        int newY = 8, newX = 1;
        if (this->motion == RANDOM) {
            newY = (rand()+20) % 15;
            newX = (rand()+20) % 15;
        }
        else {
            newY = this->lastCell.first;
            newX = this->lastCell.second;
        }
        return {newX, newY};
    }

    void Violator::moveOn(Cell where) {
        this->lastCell = {this->x,  this->y};
        this->room->setNewCoordinates(std::pair<int, int>(this->x,  this->y), where.getX(), where.getY());
        this->x = where.getX();
        this->y = where.getY();
    }

    Type Violator::whoIAm() const {
        return BADBOY;
    }
}

