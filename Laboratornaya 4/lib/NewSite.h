//
// Created by Ольга Немова on 18.12.2021.
//

#ifndef SECURITYSYSTEM_TESTS_NEWSITE_H
#define SECURITYSYSTEM_TESTS_NEWSITE_H
#include <iostream>
#include <map>
#include <cmath>
#include <string>
#include <vector>
#include <iterator>
#include "Addition.h"
#include "Cell.h"
#include "Map.h"

namespace Security {
    /**
     * \brief The class for creating the location in Algebraic Coordinate system (2D).
     * \details The new class for describing the location in Algebraic Coordinate system in terms of apart null matrix.
     * \date 22.12.2021
     * \author Nemova Olga
     *
    */
    class NewSite {
    private:
        int scale;  // Count of cells
        Map<std::pair<int, int>, Cell*> room ; // Cell's array // std::pair<int, int>
        /**
         * The default constructor. Creates the nullpointer location with maximum available count of cell for filling with subjects at this location.
         */
    public:
        NewSite ();
        /**
         * Inicialised constructor. It creates the nullpointer location with maximum available count of cell for filling with subjects at this location as the assigned parameter.
         * @param _scale : The assigned new scale for maximum count of filling elemets in this location.
         */
        explicit NewSite (int _scale);
        /**
         * Copy constructor.Creates the location alike the parameter reference.
         * @param plan : The reference for coping the all parameters for new build class.
         */
        NewSite(const NewSite& plan);

        /**
         * Destructor. For cleaning the all cells in location. It delete the all contains elements on this location.
         */
        ~NewSite() {
            room.clear();
        }
        /**
         * Setter for assings the new size of location in terms of how many elements it should contains.
         * @param _square : The new size of location in terms of int.
         */
        void setSize(int _square) {
            *this = this->Restructure(_square);
            this->scale = _square;
        }
        /**
         * Method to change the position of one cell in the whole location. Move on the coordinates of one object in other new location in it location.
         * @param point : The cell which required for transposition.
         * @param X : The new X for cell position.
         * @param Y : The new Y for cell position.
         */
        void setNewCoordinates(const std::pair<int, int>& point, int X, int Y);
        /**
         * Method returns the element (platform, people and so on), which located on this coordinates where.
         * @param cell : The location where the element is situated.
         * @return The pointer on element which located on it coordinated. If this coordinates aren't exist the method returns nullptr pointer.
         */
        const Cell* getValue(const Cell& cell) const;
        /**
         * Method returns the cell (platform, people and so on), which located on this coordinates where.
         * @param cell : The location where the element is situated.
         * @return The element which located on it coordinated.
         */
        [[nodiscard]] Cell* getCell (const Cell& cell) const;
        [[nodiscard]] Cell* getCell (Type type) const;
        /**
         * Finds the cell in all site location and returns it's coordinates.
         * @param cell : The location is required for searching.
         * @return The found cell, or otherwise the default cell.
         */
        [[nodiscard]] Condition findCell(const std::pair<int, int> &cell) const;
        /**
         * Method finds the violators only on current site location.
         * @param violator : The state of cell that you want to find it.
         * @return The Type of what is cell is was find.
         */
        [[nodiscard]] Condition findCell(Condition violator) const;
        /**
         * Method adds the new cell in the NewSite, not assign the new value on it, if it exists there alteady.
         * @param what : The new cell for adding the Map container.
         */
        void pushCell(Cell* what);
        /**
         * Method deletes cell from all site location. This method is more specialized.
         * @param X : The first coordinate of deleting cell.
         * @param Y : The second coordinate of deleting cell.
         * @param type : The type of deleting cell.
         */
        void deleteCell(int X, int Y, Condition type);
        /**
         * The operator compares two sites on containing the whole equal elements.
         * @param location : The other location for comparing.
         * @return True if sites are equal, false in otherwise.
         */
        NewSite& operator= (const NewSite& location);
        /**
         * Method finds the cell with the biggest coordinates at the current site.
         * @return : The cell object with the biggest coordinates in the current site.
         */
        [[nodiscard]] std::pair<int, int> findLong() const ;
        /**
        * Method deletes cell from all site location.
        * @param X : The first coordinate of deleting cell.
        * @param Y : The second coordinate of deleting cell.
        */
        void deleteCell(int X, int Y);
        /**
         * Show the room in console in beauty format.
         */
        void print(int );
    private:
        /**
         * Method changes the whole site in terms of changing the maximum scale of all available objects in the site. It deletes the order cell keeps the central. If the size become more then it was, it does nothing.
         * @param scale_ : The new site size scale.
         * @return The reference on the new modified Site.
         */
        NewSite& Restructure(int scale_);
    };
}



#endif //SECURITYSYSTEM_TESTS_NEWSITE_H
