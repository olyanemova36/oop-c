//
// Created by Ольга Немова on 30.11.2021.
//

#include <vector>
#include "SecuritySystem.h"
#include "NetworkModule.h"
#include "Weapon.h"

namespace Security {

    Security::SecuritySystem::SecuritySystem(Security::NewSite *room) {
        this->apartment = room;
    }

    void Security::SecuritySystem::pushStation(Platform *platform) {
        if (platform->whoIAm() == DYNAMIC) {
            std::cout << "This platform is dynamic, it can't be implements to the Security System" << std::endl;
            return;
        }
        this->stations.push_back(platform);
        this->apartment->pushCell(platform);
    }

    void SecuritySystem::killAllBadBoys() {
        std::cout << "I'm here 1" << std::endl;
        int kill = 0;
        std::vector<Cell> nearScope;
        std::cout << "Current stations" << this->stations.size() << std::endl;
        for (auto & station : this->stations) {
            std::cout << "Current slots" << station->getCurrentSlots() << std::endl;
            for ( int i = 0 ; i < station->getCurrentSlots(); i++) {
                std::cout << "I'm here 2" << std::endl;
                if (station->getModule(SENSOR, i) != nullptr) {
                    nearScope = (dynamic_cast<Sensor *>(station->getModule(SENSOR, i)))->getInformation(
                            this->apartment, *station);
                }
                if (station->getModule(WEAPON, i)  != nullptr) {
                    std::cout << "I'm here 3" << std::endl;
                    for (auto & k : nearScope) {
                        if (k.getCondition() == VIOLATOR) {
                            std::cout << "Violator:" << std::endl;
                            std::cout << k.getX() << " : " << k.getY() << std::endl;
                            (dynamic_cast<Weapon *>(station->getModule(WEAPON, i)))->killViolator((Violator&) (k), this->apartment);
                            std::cout << "OK_I_killed" << std::endl;
                            kill++;
                        }
                    }
                }
                /*if (station->getModule(NETWORK, i)!= nullptr) {
                    for(Platform robots:(((NetworkModule *)station->getModule(NETWORK, i))->getConnections())) {
                        for ( int j = 0 ; j < robots.getCurrentSlots(); i ++) {
                            if (robots.getModule(SENSOR, j) != nullptr) {
                                std::vector <Cell> nearScope = ((Sensor*)robots.getModule(SENSOR, j))->getInformation(this->apartment, *station);
                                if (robots.getModule(WEAPON, j)  != nullptr) {
                                    for (auto & k : nearScope) {
                                        if (k.getCondition() == VIOLATOR) {
                                            (dynamic_cast<Weapon *>(robots.getModule(WEAPON, j)))->killViolator((Violator&) k, this->apartment);
                                            kill++;
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }*/
            }
        }
        std::cout << "You kill " << kill << " violators!" << std::endl;
        if (this->apartment->findCell(VIOLATOR) == VIOLATOR) {
            std:: cout << "You didn't kill all violators! " << std::endl;
        }
        else {
            std:: cout << "You killed all violators! " << std::endl;
        }

    }

}
