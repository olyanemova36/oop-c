//
// Created by Ольга Немова on 24.11.2021.
//

#include "DynamicPlatform.h"


namespace Security {

    void DynamicPlatform::moveOn(Cell where) {
        this->speed = sqrt((this->x - where.getX())*(this->x - where.getX()) + (this->y - where.getY())*(this->y - where.getY()));
        this->room->setNewCoordinates(std::pair<int, int>(this->x, this->y),where.getX(), where.getY());
        std::cout << "GOOO" << std::endl;
        Violator* violator = dynamic_cast<Violator *>(this->room->getCell(BADBOY));
        std::cout << "OK I'm here" << std::endl;
        if (violator != nullptr) {
            std::cout << "OK violator go to" << std::endl;
            violator->moveOn(violator->getNextPosition());
        }
        this->x = where.getX();
        this->y = where.getY();
    }

    Type DynamicPlatform::whoIAm() const {
        return DYNAMIC;
    }

    void DynamicPlatform::setDescription(std::string description) {
        this->description = description;
    }
}
