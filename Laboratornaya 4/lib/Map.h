//
// Created by Ольга Немова on 16.12.2021.
//

#ifndef SECURITYSYSTEM_MAP_H
#define SECURITYSYSTEM_MAP_H


//
// Created by Ольга Немова on 14.12.2021.
//

#ifndef XXX_MAP_XXX
#define XXX_MAP_XXX
#include <iostream>
#include <utility>
#include <random>
#include <limits.h>
#include <memory>
#include <array>
#include <stdexcept>
#include <vector>
#include <queue>
#include "Cell.h"

namespace Security {
    /**
     * \brief The container class for heirarcial storage element's pair.
     * \date 22.12.2021
     * \author Nemova Olga
     * \details The Template Map class custom method. Creates for storage the lots of different objects in terms of pairs.
     * @tparam Key_T : The Key object, the background of all find methods.
     * @tparam Mapped_T : The Value object, the data, which should be storage there.
     */
    template<typename Key_T, typename Mapped_T>
    class Map{
    private:
        class Node;
        int current_size = 0;
        Node* head;
        int curr_height = 0;
        /**
         * \brief The intern class for Map container for storage element's information.
         * \details The intern class Node for storage the data in terms of Binary tree.
         * \date 22.12.2021
         * \author Nemova Olga
         *          */
        class Node {
        private:
            Node *right;
            Node *left;
            Node *parent;
            Key_T key;
            std::pair<Key_T, Mapped_T> pair_;
        public:
            /**
             * Inicialized constructor for creating the minimal storage quantity in Map object. It Storages the Key and Value of object.
             * @param keyed : Key object for collecting the element.
             * @param valued : The collected quantity in Map.
             */
            Node* getLeft () {
                return this->left;
            }
            Node* getRight () {
                return this->right;
            }
            Node* getParent () {
                return this->parent;
            }
            Mapped_T getValue () {
                return this->value;
            }
            Key_T getKey () {
                return this->key;
            }
            std::pair<Key_T, Mapped_T> pair() {
                return this->pair_;
            }
            void setKey(Key_T key) {
                this->key = key;
            }
            void setValue(Mapped_T value) {
                this->value = value;
            }
            void setLeft(Node * ptr) {
                this->left  = ptr;
            }
            void setRight(Node * ptr) {
                this->right = ptr;
            }
            void setParent(Node * ptr) {
                this->parent  = ptr;
            }

            void setPair(std::pair<Key_T, Mapped_T> pair__) {
                this->pair_ = pair__;
            }
            Node(Key_T keyed, Mapped_T valued) {
                this->value = valued;
                this->key = keyed;
                this->pair_ = std::pair<Key_T, Mapped_T>(key, value);
                this->left = nullptr;
                this->right = nullptr;
                this->parent = nullptr;
            };
            /**
             * Copy constructor for creating the minimal storage quantity in Map object. It Storages the Key and Value of object.
             * @param rhs : Reference on object, which characteristics it will have.
             */
            Node(const Node &rhs)
                    : key(rhs.key), value(rhs.value){
                this->pair_ = std::pair<Key_T, Mapped_T>(key, value);
                this->left = nullptr;
                this->right = nullptr;
                this->parent = nullptr;
            };
            Mapped_T value;
        };
    public:
        //Member Type
        class Iterator;
        //Constructors and Assignment
    public:
        /**
         * Default constructor for Map class. Creates the empty Binaru tree.
         */
        Map();
        /**
         * Default destructor. Delete the whole class Map object.
         */
        ~Map();
        /**
         * Returns an iterator to the beginning of the given range.
         * @return An iterator to the beginning the range.
         */
        Iterator begin() const;
        /**
         * Returns iter unchanged. The caracterized the end of central walk through the Binary tree.
         * @return iter unchanged.
         */
        Iterator end() const;
        /**
         * Public member function. Returns the number of elements in the Map container.
         * @return The number of elements in the Map container.
         */
        [[nodiscard]] size_t size() const;
        /**
         * Returns whether the Map container is empty (i.e. whether its size is 0). This function does not modify the container in any way. To clear the content of a map container, see Map::clear.
         * @return True value if the Map container is empty, and false in other case.
         */
        [[nodiscard]] bool empty() const;
        //[[nodiscard]] size_t count(const Key_T& key) const;
        /**
         * The private method to understand what the height of tree is it.
         * @return The current height of tree in Map realization through the Dinary tree.
         */
        [[nodiscard]] int current_height() const;
        /**
         * The public modifiers method.
         * Extends the container by inserting new elements, effectively increasing the container size by the number of elements inserted.
         *
         * Because element keys in a map are unique, the insertion operation checks whether each inserted element has a key equivalent to the one of an element already in the container, and if so, the element is not inserted, returning true.
         * @param item : Value to be copied to (or moved as) the inserted element.
         * @return True is insertion was success and false in other case.
         */
        bool insert( const std::pair<Key_T, Mapped_T> & item);
        /**
         * Searches the container for an element with a key equivalent to search and returns an iterator to it if found, otherwise it returns an iterator to Map::end.

Two keys are considered equivalent if the container's comparison object returns false reflexively (i.e., no matter the order in which the elements are passed as arguments).
         * @param search : Key to be searched for. Member type key_type is the type of the keys for the elements in the container, defined in map as an alias of its first template parameter (Key).
         * @return An iterator to the element, if an element with specified key is found, or map::end otherwise.
         */
        Iterator find(const Key_T &search) const;
        /**
         * Removes all elements from the map container (which are destroyed), leaving the container with a size of 0.
         */
        void clear() noexcept;
        /**
         * Removes from the Map container either a single element or a range of elements ([first,last)).
         * @param it : Iterator pointing to a single element to be removed from the Map. This shall point to a valid and dereferenceable element.
         */
        void erase (Iterator it);
        /**
         * Removes from the Map container either a single element or a range of elements ([first,last)).
         * @param first : Iterators specifying a begin of the range within the map container to be removed: [first,last).
         * @param second : Iterators specifying a end of the range within the map container to be removed: [first,last).
         */
        void erase (Iterator first, Iterator second);
        /**
         * Returns a reference to the mapped value of the element identified with key key.
         * @param key : Key value of the element whose mapped value is accessed.
         * @return A reference to the mapped value of the element with a key value equivalent to k.
         */
        void erase (const Key_T& key);
        Mapped_T& at(const Key_T& key);
        /**
         * A void private method to show the structure the storage of Map container.
         */
        void show();
        /**
         * Returns a reference to the mapped value of the element identified with key key.
         * @param key : Key value of the element whose mapped value is accessed.
         * @return A reference to the mapped value of the element with a key value equivalent to k.
         */
        const Mapped_T& at( const Key_T& key ) const;
        /**
         * If k matches the key of an element in the container, the function returns a reference to its mapped value.

If k does not match the key of any element in the container, the function inserts a new element with that key and returns a reference to its mapped value. Notice that this always increases the container size by one, even if no mapped value is assigned to the element (the element is constructed using its default constructor).

A similar member function, Map::at, has the same behavior when an element with the key exists, but throws an exception when it does not.
         * @return A reference to the mapped value of the element with a key value equivalent to k.
         */
        Mapped_T& operator[](const Key_T& ) noexcept;
        /**
         * If k matches the key of an element in the container, the function returns a reference to its mapped value. If k does not match the key of any element in the container, the function inserts a new element with that key and returns a reference to its mapped value. Notice that this always increases the container size by one, even if no mapped value is assigned to the element (the element is constructed using its default constructor). A similar member function, Map::at, has the same behavior when an element with the key exists, but throws an exception when it does not.
         * @return
A reference to the mapped value of the element with a key value equivalent to k.
         */
        const Mapped_T& operator[](const Key_T& ) const noexcept;
        //Map<Key_T, Mapped_T> operator=(const Map<Key_T, Mapped_T>& ) const noexcept;
    private:
        //Size
        /**
         * A void private method to show the structure the storage of Map container.
         */
        void show(Node *);
        /**
         * A private method for container. Removes from the Map container either a single element or a range of elements ([first,last)).
         * @return True if the erasing was success, and false if deleting key is not exist in tree.
         */
        bool erase_(const Key_T&);
        /**
         * A private method to check if the Node in Map container realization is Leaf, which in left and right children has only a null pointers.
         * @param node : Checking node.
         * @return True if node is leaf, has the null pointers on left and right children, and false in other case.
         */
        bool isLeaf( Node* node);
        /**
         * A private method to clean the Map container begin from node pointer.
         * @param begin : Begin Node for whole deleting the tree from the root.
         */
        void destroy_tree(Node * begin);
        /**
         * A private method for deleting the tree in Map container realization.
         */
        void destroy_tree();
        /**
         * Searches the container for an element with a key equivalent to search and returns an Node pointer to it if found, otherwise it returns an null pointer. Two keys are considered equivalent if the container's comparison object returns false reflexively (i.e., no matter the order in which the elements are passed as arguments).
         * @param key : Key to be searched for. Member type key_type is the type of the keys for the elements in the container, defined in map as an alias of its first template parameter (Key).
         * @param root : The root of Searching tree.
         * @return An Node pointer to the element, if an element with specified key is found, or null pointer otherwise.
         */
        Node* find_(const Key_T & key, Node* root);
        /**
         * A private method for searching the element for node deleting. Finds the in the left subtree the max key value element and returns Node pointer on it.
         * @param node : Node pointer for which the left subtree should has the max key value element.
         * @return Node pointer at found element.
         */
        Node* leftMax(Node* node);
        /**
         * A private special method for deleting the node element in Binary tree.
         * @param root : Root pointer in Binary tree, in which the deleting element is located.
         * @param d_node : Deleting element in Map container binary tree.
         */
        void deletDeepest(Node* root, Node* d_node);
        /**
         * Private overloaded operator for checking the equality of two Map containers. It compares every element in Map container, not direct on order of elements in Map container.
         * @return True, if Map containers are equal, and false otherwise.
         */
        bool operator==(const Map<Key_T, Mapped_T>&) const;
    public:
        /**
         * \brief The iterable class for Map container for managing through the elements pairs.
         * \date 22.12.2021
         * \author Nemova Olga
         * \details class for managing the storage elements in Map.
         */
        class Iterator{
        private:
            Node* target;
            std::queue<Node*> que;
            /**
             * The method for filling the queue of Iterator field in central walking terms through the Binary tree.
             * @param node : Root pointer in Binary search tree.
             */
            void BreathFill(Node* node) {
                if (node== nullptr) {
                    return;
                }
                this->que.push(node);
                BreathFill(node->getLeft());
                BreathFill(node->getRight());
            }
            /**
             * The method for cleaning the queue of Iterator due to the fill it with new element of new search in engaged target field.
             */
            void createList() {
                while(!this->que.empty()) {
                    this->que.pop();
                }
                BreathFill(this->target);
            }
            /**
             * Getter. Returns the head of Iterator pointer.
             * @return Pointer on the head of Map Binary tree.
             */
        public:
            /**
             * The default constructor for Iterator class. It appoints the target pointer at null value pointer.
             */
            Iterator() {
                this->target = nullptr;
            }
            /**
             * The Inicilized constructor. It appoints the target pointer at parameter Binary tree root pointer of Map container.
             * @param pos : Root position in Binary tree or the pointer on the subtree in its tree.
             */
            Iterator(Node* pos){
                this->target = pos;
                BreathFill(this->target);
            };
            Iterator(const Iterator& it){
                if (this->target == it.target) {
                }
                else {
                    this->target = it.target;
                    BreathFill(this->target);
                }
            };
            /**
             * Operator for checking if the comparing Iterators has the equal pointer target.
             * @param lhs : The first Iterator for comparing.
             * @param rhs : The second Iterator for comparing.
             * @return True if Iterators has the equal pointers on the same memory, and false otherwise.
             */
            bool operator==(const Iterator &rhs){
                return (this->target == rhs.target);
            };
            /**
             * Operator for checking if the comparing Iterators has the not equal pointer target.
             * @param lhs : The first Iterator for comparing.
             * @param rhs : The second Iterator for comparing.
             * @return True if Iterators has the equal pointers on the same memory, and false otherwise.
             */
            bool operator!=( const Iterator &rhs){
                return (this->target != rhs.target);
            };

            /**
             * Prefix increment for allocating through the Binary tree. Manage the location of pointer with help of the central walking.
             * @return Change the Iterator position on next in the Binary tree. Reference on this Iterator position in Binary tree.
             */
            Iterator& operator++(){
                if(!this->que.empty()) {
                    Node* node = this->que.front();
                    while (this->target == node) {
                        this->que.pop();
                        node = this->que.front();
                    }
                    this->target = node;
                    this->que.pop();
                    return *(this);
                }
                this->target = nullptr;
                return *this;
            };

            /**
             * Postfix increment for allocating through the Binary tree. Manage the location of pointer with help of the central walking.
             * @return Reference on the Iterator position in Binary tree. Change the Iterator position on next in the Binary tree.
             */
            Iterator operator++(int){
                Iterator c_ = *this;
                ++(*this);
                return c_;
            };
            /**
             * Assignment operator. Assign the current Iterator on the pointer of it Iterator position in the tree.
             * @param it : The new pointer for Iterator target field.
             * @return Reference on the Iterator position in other assign tree.
             */
            Iterator& operator=(const Iterator& it) {
                if (*this == it) {
                    return *(this);
                }
                this->target = it.target;
                this->createList();
                return *this;
            }
            /**
             * Referring to a field by reference. This returns the pair of contains elements in Map container Node with Key and value type.
             * @return The pair of contains elements in Map container Node with Key and value type.
             */
            std::pair<Key_T, Mapped_T> *operator->() {
                std::pair<Key_T, Mapped_T>* pair_t = new std::pair<Key_T, Mapped_T>(this->target->getKey(), this->target->getValue());
                return pair_t;

            };
            /**
             * Rereference operator. This returns the pair of contains elements in Map container Node with Key and value type.
             * @return The pair of contains elements in Map container Node with Key and value type.
             */
            std::pair<Key_T, Mapped_T> &operator*() {
                auto *pair =  new std::pair<Key_T, Mapped_T>(this->target->getKey(), this->target->getValue());
                return pair;
            };
            Node * getHead() {
                return this->target;
            }
        };
    };

    //Map Default Constructor
    template<typename Key_T, typename Mapped_T>
    Map<Key_T, Mapped_T>::Map(){
        this->head = nullptr;
        this->current_size = 0;
        this->curr_height = 0;
    }

    //Map destructor
    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::destroy_tree(Node *leaf) {
        if(leaf != nullptr) {
            destroy_tree(leaf->getLeft());
            destroy_tree(leaf->getRight());
            delete leaf;
        }
    }
    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::destroy_tree() {
        destroy_tree(head);
    }

    template<typename Key_T, typename Mapped_T>
    bool Map<Key_T, Mapped_T>::insert( const std::pair<Key_T, Mapped_T> & item) {
        Node* pioneer = new Node(item.first, item.second);
        Node* prev;
        if (this->head == nullptr) {
            this->head = pioneer;
            this->curr_height++;
            this->current_size++;
            return true;
        }
        else {
            Node* tmp;
            tmp = head;
            while (tmp != nullptr) {
                prev = tmp;
                if (item.first > tmp->getKey()) {
                    tmp = tmp->getRight();
                }
                else if (item.first == tmp->getKey()) {
                    throw std::out_of_range("Key is already exist in map");
                }
                else {
                    tmp = tmp->getLeft();
                }
            }
            if (isLeaf(prev)) {
                this->curr_height++;
                this->current_size++;
            }
            else {
                this->current_size++;
            }
            if (item.first < prev->getKey()) {
                prev->setLeft(pioneer);
                pioneer->setParent(prev);
            }
            else {
                prev->setRight(pioneer);
                pioneer->setParent(prev);
            }
        }
        return true;
    };
    template<typename Key_T, typename Mapped_T>
    Map<Key_T, Mapped_T>::~Map(){
        destroy_tree();
    }
    template<typename Key_T, typename Mapped_T>
    bool Map<Key_T, Mapped_T>::isLeaf( Node* node) {
        if (node == nullptr) {
            return false;
        }
        return ((node->getLeft() == nullptr) && (node->getRight()== nullptr));
    }
    template<typename Key_T, typename Mapped_T>
    [[nodiscard]] int Map<Key_T, Mapped_T>::current_height() const{
        return this->curr_height;
    };
    //Iteratorsn()
    template<typename Key_T, typename Mapped_T>
    typename Map<Key_T, Mapped_T>::Iterator Map<Key_T, Mapped_T>::begin() const {
        Iterator it(head);
        return it;
    }
    template<typename Key_T, typename Mapped_T>
    typename Map<Key_T, Mapped_T>::Iterator Map<Key_T, Mapped_T>::end() const {
        return Iterator(nullptr);
    }
    //iterator find
    template<typename Key_T, typename Mapped_T>
    typename Map<Key_T, Mapped_T>::Iterator Map<Key_T, Mapped_T>::find(const Key_T &search) const {
        Node* tmp = this->head;
        if (tmp != nullptr) {
            while (tmp->getParent()!= nullptr ) {
                tmp = tmp->getParent();
            }
            while (tmp != nullptr) {
                if(tmp->getKey() == search){
                    return Iterator(tmp);
                }
                else if (search > tmp->getKey()) {
                    tmp = tmp->getRight();
                }
                else if (search < tmp->getKey()) {
                    tmp = tmp->getLeft();
                }
            }
        }
        return end();
    }

    //Size
    template<typename Key_T, typename Mapped_T>
    size_t Map<Key_T, Mapped_T>::size() const{
        return this->current_size;
    }

    //Empty
    template<typename Key_T, typename Mapped_T>
    bool Map<Key_T, Mapped_T>::empty() const {
        return this->head == nullptr;
    }

    template<typename Key_T, typename Mapped_T>
    typename Map<Key_T, Mapped_T>::Node *Map<Key_T, Mapped_T>::find_(const Key_T & item, Node* leaf) {
        if (leaf != nullptr) {
            if (item ==leaf->getKey()) {
                return leaf;
            }
            if (item < leaf->getKey()) {
                return find_(item, leaf->getLeft());
            }
            else {
                return find_(item, leaf->getRight());
            }
        }else {
            return nullptr;
        }
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::clear() noexcept {
        destroy_tree();
        this->head = nullptr;
    }

    template<typename Key_T, typename Mapped_T>
    const Mapped_T &Map<Key_T, Mapped_T>::at(const Key_T &key) const {
        const Node* find = find_(key, this->head);
        if (find == nullptr) {
            throw std::out_of_range("Key is not found in map");
        }
        return static_cast<Mapped_T &>(find->getValue());
    }

    template<typename Key_T, typename Mapped_T>
    Mapped_T& Map<Key_T, Mapped_T>::at(const Key_T &key) {
        Node* find = find_(key, this->head);
        if (find == nullptr) {
            throw std::out_of_range("Key is not found in map");
        }
        return static_cast<Mapped_T &>(find->getValue());
    }

    template<typename Key_T, typename Mapped_T>
    typename Map<Key_T, Mapped_T>::Node *Map<Key_T, Mapped_T>::leftMax(Node* node) {
        if (node == nullptr) {
            return nullptr;
        }
        else {
            while(node->getRight()!= nullptr) {
                node = node->getRight();
            }
            return node;
        }
    }

    template<typename Key_T, typename Mapped_T>
    bool Map<Key_T, Mapped_T>::erase_(const Key_T& key) {
        if (head == nullptr) {
            return false;
        }
        if (isLeaf(head)) {
            if (head->getKey() == key) {
                this->head = nullptr;
                return true;
            }
            else {
                return false;
            }
        }

        std::queue<Node*> q;
        q.push(head);

        Node* temp;
        Node* key_node = nullptr;

        while (!q.empty()) {
            temp = q.front();
            q.pop();

            if (temp->getKey() == key)
                key_node = temp;

            if (temp->getLeft())
                q.push(temp->getLeft());

            if (temp->getRight())
                q.push(temp->getRight());
        }

        if (key_node != nullptr) {
            Key_T x = temp->getKey();
            Mapped_T y = temp->getValue();
            std::pair<Key_T, Mapped_T> pair = temp->pair();
            deletDeepest(head, temp);
            key_node->setKey(x);
            key_node->setValue(y);
            key_node->setPair(pair);
            return true;
        }
        return false;
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::deletDeepest(Map::Node *root, Map::Node *d_node) {
        std::queue<Node*> q;
        q.push(this->head);
        // Do level order traversal until last node
        struct Node* temp;
        while (!q.empty()) {
            temp = q.front();
            q.pop();
            if (temp == d_node) {
                temp = nullptr;
                delete (d_node);
                return;
            }
            if (temp->getRight()) {
                if (temp->getRight() == d_node) {
                    temp->setRight(nullptr);
                    delete (d_node);
                    return;
                }
                else
                    q.push(temp->getRight());
            }

            if (temp->getLeft()) {
                if (temp->getLeft() == d_node) {
                    temp->setLeft(nullptr);
                    delete (d_node);
                    return;
                }
                else
                    q.push(temp->getLeft());
            }
        }

    }

    template<typename Key_T, typename Mapped_T>
    Mapped_T &Map<Key_T, Mapped_T>::operator[](const Key_T& key) noexcept {
        Node* find = this->find_(key, head);
        if (find == nullptr) {
        }
        else {
            return find->value;
        }
    }
    template<typename Key_T, typename Mapped_T>
    const Mapped_T &Map<Key_T, Mapped_T>::operator[](const Key_T& key) const noexcept {
        const Node* find = this->find_(key, head);
        if (find == nullptr) {
        }
        else {
            return find->value;
        }
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::erase(Map::Iterator it) {
        Node* find = find_(it.getHead()->getKey(), it.getHead());
        if (find == nullptr) {
            return;
        }
        else {
            erase_(it.getHead()->getKey());
        }
    }
    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::show() {
        show(head);
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::show(Node* node) {
        if (node == nullptr) {
            return;
        }
        show(node->getLeft());
        show(node->getRight());
        std::cout << node->getKey() << " :: " << node->getValue() << std::endl;
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::erase(Map::Iterator, Map::Iterator) {
        Map<Key_T, Mapped_T>::Iterator it;
        for (it = this->begin(); it != this->end(); ++it) {
            this->erase(it);
        }
    }

    template<typename Key_T, typename Mapped_T>
    void Map<Key_T, Mapped_T>::erase(const Key_T &key) {
        this->erase_(key);
    }


    /*template<typename Key_T, typename Mapped_T>
    bool Map<Key_T, Mapped_T>::operator==(const Map<Key_T, Mapped_T>& other) const {
        if ((this->current_size != other.current_size) || (this->curr_height != other.curr_height)) {
            return false;
        }
        Map::Iterator it;
        int counter = 0;
        for ( it = this->begin(); it != this->end(); it++) {
            if(it->second == other[it->first]) {
                counter++;
            }
        }
        return counter == this->current_size;
    }*/
}


#endif


#endif //SECURITYSYSTEM_MAP_H
