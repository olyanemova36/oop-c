//
// Created by Ольга Немова on 24.11.2021.
//

#ifndef SECURITYSYSTEM_NETWORKMODULE_H
#define SECURITYSYSTEM_NETWORKMODULE_H
#include "Module.h"
#include <vector>
#include <list>
#include "Platform.h"
#include "Sensor.h"
namespace Security {
    /**
     * \brief The Heir class for realisating the Platform interface in connecting terms.
     * \details The one of Module types for creating the net of different Security platforms in room.
     * \date 22.12.2021
     * \author Nemova Olga
     *
     * */
    class NetworkModule: public Module {
    private:
        int sessions = 0;
        int max_sessions;
        std::vector<Platform> activeCommutation;
    public:
        /**
         * Th default constructor. Which assign the default quantities with range of action equals like 3 on all fields in class.
         */
        NetworkModule();
        /**
         * The Inicialized constructor, which creates the turn on or turn down Network modules.
         * @param turnOn : The state on turning on of Network module. If it is turn down you can't use it.
         */
        explicit NetworkModule (bool turnOn);
        /**
         * Setter which assign the new scale of range of action the module. It describes the field on which it can connected with other Platforms.
         * @param c : The new radius of acting through the location.
         */
        void setRangeOfAction(int c) override;
        /**
         * Getter which returns the what state of charging this module has.
         * @return The state of charging in bool terms, if it is charging it is not active.
         */
        bool getCondition();
        /**
         * Getter which returns the value of Range of Action to understand what hear filed is available for it.
         * @return The Radius of acting in this assign location.
         */
        int getRangeOfAction();
        /**
         * Getter returns the count of current sessions with other available platforms in near location.
         * @return Count of connections with other available platforms.
         */
        int getSessions();
        /**
         * The Postfix increment method to increase the count of available count of maximum connections with other platforms.
         */
        void increaseSessions();
        /**
         * Method for specifiing the type of Module is it.
         * @return The type of Module is it.
         */
        virtual Type whoIAm() const;
        /**
         * Method returns the list of all available pairs Platforms for connecting in terms of range of acting of this module.
         * @param platform : The reference on the platform in which the module located.
         * @return The list of all platforms which are available for connection.
         */
        [[nodiscard]] std::vector<Platform>getAvailablePairs(Platform* platform) const;
        /**
         * Method returns the list of all available pairs of its neighbour's Platforms for connecting in terms of range of acting of this module (not neighbour).
         * @param net : The reference on the neighbour's platform in which the module located.
         * @return  The list of all platforms which are available for connection for neighbour.
         */
        [[nodiscard]] std::vector<Platform>getNeighborPairs(Platform* net);
        std::vector<Platform> getConnections ();
        /**
        * The private method for building the net of connection in terms of Network Module for only one connection.
        * @param platform : The platform with with we want to connect as the result.
        * @param from : The platform begin from which we connecting.
        * @return True if connection was success and false in otherwise.
        */
        bool connectTo(Platform *platform, Platform *from);
        /**
         * The private method for building the net of connection in terms of Network Module for two connections.
         * @param middle : The middle helper for connection.
         * @param end : The platform with with we want to connect as the result.
         * @param from : The platform begin from which we connecting.
         * @return True if connection was success and false in otherwise.
         */
        bool connectTo(Platform *middle, Platform *end, Platform *from);
    private:
        /**
         * Method create the connection with other Network Module platform module and creates the net of connections through the location field.
         * @param end : The pointer at other Platform with which you want to connect.
         * @return True if connection was success and false in otherwise.
         */
        bool createConnection(NetworkModule* end);
        /**
         * Method create the connection with other Network Module platform module and the middle module, which has the middle module as neighbour, and creates the net of connections through the location field.
         * @param middle : The middle Network Module in complex connection.
         * @param end : The end Network Module with which we want to connect as the result.
         * @return True if connection was success and false in otherwise.
         */
        bool createConnection(NetworkModule* middle, NetworkModule* end);


    };
}



#endif //SECURITYSYSTEM_NETWORKMODULE_H
