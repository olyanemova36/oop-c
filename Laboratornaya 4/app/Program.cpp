//
// Created by Ольга Немова on 08.12.2021.
//
//
// Created by Ольга Немова on 08.11.2021.
//

#include <strstream>
#include <iostream>
#include <cmath>
#include <string>

#define _CRTDBG_MAP_ALLOC
#include <cstdlib>

#ifdef _DEBUG
define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif
#include <iostream>
#include <ncurses.h>
#include <NewSite.h>
#include <Violator.h>
#include <Platform.h>
#include <DynamicPlatform.h>
#include <SecuritySystem.h>
#include <Weapon.h>

enum errorType {
    ok,
    incorrectInput,
    eof
};

enum contactType {
    in = 0,
    out = 1,
    notStated = -1
};

template <class Get>
int getNum(Get&);
static void finish(int sig);
static void defaultRoom(Security::NewSite* room);
using namespace Security;
template <typename T>
errorType input (T &a);
template <typename T>
void checkInput (T &a);



int main(){
    NewSite room(50);
    SecuritySystem GLONAS(&room);
    defaultRoom(&room);
    Security::Platform station(Cell( 11, 7, ENGAGED));
    Security::Sensor EYEOCUMO;
    Security::NetworkModule XV678(true);
    Security::Weapon KILLER2000(true);
    station.pushModule(EYEOCUMO);
    station.pushModule(KILLER2000);
    station.pushModule(XV678);

    GLONAS.pushStation(&station);
    Security::DynamicPlatform robot(Cell( 5, 4, ENGAGED), &room);
    Cell* robot_ = &robot;
    room.pushCell(robot_);
    int a = 0;
    short c1, c2;
    std::cout << "Hello! There is a box for Security System managing!" << std::endl;
    do {
        std::cout << "0 :: Quit;" << std::endl; //OK
        std::cout << "1 :: Add new STATION (D300, G400);" << std::endl; //OK
        std::cout << "2 :: Add new ROBOT (BABYROBOT100, TERMINATOR37);" << std::endl; //Little
        std::cout << "3 :: Create new connection between stations;" << std::endl;
        //std::cout << "4 :: Check the territory;" << std::endl;
        std::cout << "5 :: Move ROBOT;" << std::endl; //OK
        std::cout << "6 :: Push module in station;" << std::endl;
        std::cout << "7 :: Kill all violators;" << std::endl; //OK
        std::cout << "8 :: Show me the room;" << std::endl; //OK
        std::cout << "Make your choice: --> ";
        checkInput(a);
        std::cout << std::endl;
        try {
            switch (a) {
                case 1: {
                    std::string desc;
                    std::cout << "Enter the name for new station >"  << std::endl;
                    int x, y;
                    std::cin >> desc;
                    std::cout << "Enter the coordinates for new station >\nX=";
                    std::cin >> x;
                    std::cout << "Y=";
                    std::cin >> y;
                    Security::Platform station(Cell( x, y, ENGAGED));
                    station.setDescription(desc);
                    GLONAS.pushStation(&station);
                    std::cout << "Success"  << std::endl;
                }
                    break;
                case 2: {
                    std::string desc;
                    std::cout << "Enter the name for new robot >"  << std::endl;
                    int x, y;
                    std::cin >> desc;
                    std::cout << "Enter the coordinates for new robot >\nX=";
                    std::cin >> x;
                    std::cout << "Y=";
                    std::cin >> y;
                    Security::DynamicPlatform robot1( Cell( x, y, ENGAGED), &room);
                    room.pushCell(&robot1);
                    std::cout << "Success"  << std::endl;
                }
                    break;
                case 3: {
                    std::cout << "We have only the one robot platform there connect to it " << std::endl;
                    std::cout << "If it hasn't required modules connection can't be processing..."<< std::endl;
                    ((NetworkModule *)station.getModule(Security::NETWORK, 0))->connectTo(&station, &robot);
                    std::cout << "Success"  << std::endl;
                }
                    break;
                case 4: {
                    unsigned short t;
                    std::cout << "Enter type of contact (0 for in, 1 for out): --> ";
                    checkInput(t);
                    //std::cout << p.groupOfContacts(t);
                }
                    break;
                case 5: {
                    DynamicPlatform* robot1 = dynamic_cast<DynamicPlatform*>(room.getCell(DYNAMIC));
                    if (robot1 != nullptr) {
                        std::cout << "The robot has the coordinates: { " << robot1->getX() << " : " << robot1->getY() << " }" << std::endl;
                        int x, y;
                        std::cout << "Enter the coordinates for new robot >\nX=";
                        std::cin >> x;
                        std::cout << "Y=";
                        std::cin >> y;
                        robot1->moveOn(Cell(x, y));
                    }
                    else {
                        std::cout << "In this location there sre not ary robot!" << std::endl;
                    }


                }
                    break;
                case 6: {
                    std::cout << "What type of module do you want to : --> \n1. Sensor;\n2. Weapon;\n3. NetworkModule;";
                    checkInput(c1);
                    switch (c1){
                        case 1: {
                            Security::Sensor EYEOCUMO_baby;
                            DynamicPlatform* robot_ = dynamic_cast<DynamicPlatform *>(room.getCell(DYNAMIC));
                            robot_->pushModule(EYEOCUMO_baby);
                            std::cout << "Success"  << std::endl;
                            break;
                        }
                        case 2: {
                            Security::Weapon KILLER2000(true);
                            DynamicPlatform* robot__ = dynamic_cast<DynamicPlatform *>(room.getCell(DYNAMIC));
                            robot__->pushModule(KILLER2000);
                            std::cout << "Success"  << std::endl;
                            break;
                        }
                        case 3: {
                            Security::NetworkModule XV678_baby(true);
                            DynamicPlatform* robot__ = dynamic_cast<DynamicPlatform *>(room.getCell(DYNAMIC));
                            robot__->pushModule(XV678_baby);
                            std::cout << "Success"  << std::endl;
                            break;
                        }
                    }

                }
                    break;
                case 7: {
                     GLONAS.killAllBadBoys();
                }
                    break;
                case 0: {
                    break;
                }
                case 8: {
                    room.print(15);
                    break;
                }
                default: {
                    std::cout << "Enter only numbers from 0 to 7!" << std::endl;
                }
                    break;
            }
        }
        catch (std::exception &e) {
            std::cout << e.what();
        }
        std::cout << std::endl;
    } while (a != 0);

}
static void defaultRoom(NewSite* room) {
    room->pushCell(new Cell(3, 2, BARRIER));
    room->pushCell(new Cell(3, 5, BARRIER));
    room->pushCell(new Cell(3, 6, BARRIER));
    room->pushCell(new Cell(3, 7, BARRIER));
    room->pushCell(new Cell(3, 8, BARRIER));
    room->pushCell(new Cell(3, 9, BARRIER));
    room->pushCell(new Cell(3, 10, BARRIER));
    room->pushCell(new Cell(4, 10, BARRIER));
    room->pushCell(new Cell(5, 10, BARRIER));
    room->pushCell(new Cell(6, 10, BARRIER));
    room->pushCell(new Cell(7, 10, BARRIER));

    room->pushCell(new Cell(6, 5, BARRIER));
    room->pushCell(new Cell(7, 5, BARRIER));
    room->pushCell(new Cell(8, 5, BARRIER));
    room->pushCell(new Cell(9, 5, BARRIER));
    room->pushCell(new Cell(10, 5, BARRIER));

    room->pushCell(new Cell(9, 14, BARRIER));
    room->pushCell(new Cell(10, 14, BARRIER));
    room->pushCell(new Cell(11, 14, BARRIER));
    room->pushCell(new Cell(11, 13, BARRIER));
    room->pushCell(new Cell(11, 12, BARRIER));
    room->pushCell(new Cell(12, 12, BARRIER));
    room->pushCell(new Cell(13, 12, BARRIER));
    room->pushCell(new Cell(13, 11, BARRIER));
    room->pushCell(new Cell(13, 10, BARRIER));
    room->pushCell(new Cell(14, 10, BARRIER));
    room->pushCell(new Cell(14, 9, BARRIER));
    room->pushCell(new Cell(14, 8, BARRIER));
    room->pushCell(new Cell(14, 7, BARRIER));
    room->pushCell(new Cell(14, 6, BARRIER));
    room->pushCell(new Cell(14, 5, BARRIER));
    room->pushCell(new Cell(14, 4, BARRIER));
    room->pushCell(new Cell(14, 3, BARRIER));

    room->pushCell(new Cell(10, 8, Security::VIOLATOR));
    room->pushCell(new Cell(5, 13, Security::VIOLATOR));
    room->pushCell(new Cell(11, 10, Security::VIOLATOR));

}
template <class Get>
int getNum(Get& a) {
    std::cin >> a;
    if (!std::cin.good()) {
        return 0;
    }
    return 1;
}
template <class T>
errorType input (T &a){
    std::cin >> a;
    if (!std::cin.good()) {
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        return incorrectInput;
    }
    if (std::cin.eof())
        return eof;
    return ok;
}

template <class T>
void checkInput (T &a) {
    errorType error;
    do {
        error = input(a);
        if (error == eof || error == incorrectInput)
            std::cout << "Enter only numbers, please!" << std::endl;
    } while (error != ok);
}

