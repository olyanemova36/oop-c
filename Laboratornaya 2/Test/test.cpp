#include "pch.h"
#include "Diocle.h"
#include "gtest\gtest.h"
#include <math.h>

TEST(DiocleConstructor, DefaultConstructor)
{
	Polar::Diocle F;
	ASSERT_EQ(0, F.getCenter().x);
	ASSERT_EQ(0, F.getCenter().y);
	ASSERT_EQ(1, F.qetAssimpt());
}

TEST(DiocleConstructor, InitConstructors)
{
	Polar::Diocle F(3.0);
	Polar::Point p(2.0, 3.0);
	ASSERT_EQ(3.0, F.qetAssimpt());
	ASSERT_EQ(0.0, F.getCenter().y);
	ASSERT_EQ(0.0, F.getCenter().x);

	Polar::Diocle F1(p, 8.0);
	ASSERT_EQ(2.0, F1.getCenter().x);
	ASSERT_EQ(3.0, F1.getCenter().y);
	ASSERT_EQ(8.0, F1.qetAssimpt());

	Polar::Diocle F2(-4.0, -6.0, 3.0);
	ASSERT_EQ(-4.0, F2.getCenter().x);
	ASSERT_EQ(-6.0, F2.getCenter().y);
	ASSERT_EQ(3.0, F2.qetAssimpt());
}

TEST(DiocleConstructor, TestException)
{
	Polar::Point p1;
	Polar::Diocle Cicloide(1., 1, 0.);
	ASSERT_ANY_THROW(Cicloide.parable());
	//ASSERT_ANY_THROW(Cicloide.path(acosf(0.0)));
}

TEST(DiocleMethods, Setters) {
	Polar::Diocle F5;
	Polar::Point p5(3.0, 2.0);
	F5.setPoint(p5);
	ASSERT_EQ(3.0, F5.getCenter().x);
	ASSERT_EQ(2.0, F5.getCenter().y);
	Polar::Point p6(-3.0, -2.0);
	F5.setPoint(p6);
	ASSERT_EQ(-3.0, F5.getCenter().x);
	ASSERT_EQ(-2.0, F5.getCenter().y);
	F5.setAssimpt(2.0);
	ASSERT_EQ(2.0, F5.qetAssimpt());
}

TEST(DiocleMethods, Parameters)
{
	Polar::Diocle F;
	const double err = 0.001;
	ASSERT_NEAR(3 * PI, F.square(), err);
	ASSERT_NEAR(10 * PI * PI, F.volume(), err);
	ASSERT_EQ(2, F.assim());
	ASSERT_EQ(1, F.ordinate(1.0).x);
	ASSERT_EQ(-1.0, F.ordinate(1.0).y);
	ASSERT_EQ(0, F.ordinate(0).x);
	ASSERT_EQ(0, F.ordinate(0).y);
	ASSERT_EQ((2 * sin(3 * PI) * sin(3 * PI) / cos(3 * PI)), F.path(3 * PI));
	ASSERT_NEAR(0.0, F.path(0.0), err);
	ASSERT_EQ(-0.25, F.parable().a);
	ASSERT_EQ(0, F.parable().b);
	ASSERT_EQ(0, F.parable().c);
	ASSERT_ANY_THROW(F.ordinate(2.0));

	Polar::Diocle F1(1., 3., 2.);
	ASSERT_NEAR(PI * 3 * 2, F1.square(), err);
	ASSERT_NEAR(10 * PI * PI * 2 * 2 * 2, F1.volume(), err);
	ASSERT_EQ(3.0, F1.ordinate(1.0).x);
	ASSERT_EQ(3.0, F1.ordinate(1.0).y);
	ASSERT_EQ((sqrt(1.0 / (2 * 2.0 - 1.0)) + 3.0), F1.ordinate(2.0).x);
	ASSERT_EQ((-sqrt(1.0 / (2 * 2.0 - 1.0)) + 3.0), F1.ordinate(2.0).y);
	ASSERT_EQ(2 * 2.0 * sin(3 * PI) * sin(3 * PI) / cos(3 * PI), F1.path(3 * PI));
	ASSERT_EQ(0, F1.path(0));
	ASSERT_EQ(-0.5, F1.parable().a);
	ASSERT_EQ(0.75, F1.parable().b);
	ASSERT_EQ(5.5, F1.parable().c);
	ASSERT_ANY_THROW(F1.ordinate(5.0));

}