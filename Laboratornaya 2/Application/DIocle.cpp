﻿// DIocle.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include "Diocle.h"
#include <iostream>
int main(int argc, const char* argv[]) {
	Polar::Diocle Cucloide(0.0, 0.0, 4.0);
	Polar::Point p(3);
	int fl1 = 1;

	double assimpt;
	while (fl1) {
		std::cout << "Your circle function has these parameters:" << std::endl;
		std::cout << "Area: " << Cucloide.square() << std::endl;
		std::cout << "Volume: " << Cucloide.volume() << std::endl;
		std::cout << "Meaning of assimpt:" << Cucloide.assim() << std::endl;
		try {
			Polar::Parabola Equ = Cucloide.parable();
			std::cout << "Parabola: " << Equ.a << "*y^2 " << Equ.b << "*y " << Equ.c << std::endl;
		}
		catch (std::exception& ex) {
			std::cout << ex.what() << std::endl;
		}
		int fl2 = 1;
		while (fl2) {
			std::cout << "Enter x for calculate value y(x) or press ctrl+Z to quit:" << std::endl;
			double x;
			Polar::Point y;
			std::cin >> x;
			fl2 = std::cin.good();
			if (!fl2)
				continue;
			try {
				y = Cucloide.ordinate(x);
				std::cout << "y.x = " << y.x << ", y2 = " << y.y << std::endl;
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}

		}
		int fl3 = 1;
		while (fl3) {
			std::cout << "Enter fi for calculate value path to center or press ctrl+Z to quit:" << std::endl;
			double fi;
			std::cin >> fi;
			fl3 = std::cin.good();
			if (!fl3)
				continue;
			try {
				double path = Cucloide.path(fi);
				std::cout << "path to center = " << path << std::endl;
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}

		}
		std::cin.clear();
		std::cout << "Enter new x, y and assimpt to continue or press ctrl+X to quit:" << std::endl;
		std::cin >> p.x >> p.y >> assimpt;
		if (std::cin.good()) {
			try {
				Cucloide.setPoint(p);
			}
			catch (std::exception& ex) {
				std::cout << ex.what() << std::endl;
			}
		}
		else fl1 = 0;
	}

	_CrtDumpMemoryLeaks();
	return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
