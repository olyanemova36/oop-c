#define _CRTDBG_MAP_ALLOC
#include <strstream>
#include <stdio.h>
#include <string.h>
#include "Diocle.h"
#include <iostream>
#include <crtdbg.h>

namespace Polar {

	Point Diocle::ordinate(double x) const {
		double dx = x - center.x;
		if (2 * this->assimpt - dx == 0.0)
			throw std::exception("illegal argument x");
		dx = dx * dx * dx / (2 * this->assimpt - dx);
		if (dx < 0)
			throw std::exception("illegal argument x");
		dx = sqrt(dx);
		Point res;
		res.x = center.y + dx;
		res.y = center.y - dx;
		return res;
	}

	double Diocle::path(double fi) const {
		if (cos(fi) == 0.0)
			throw std::exception("illegals argument x");
		return 2 * this->assimpt * sin(fi) * sin(fi) / cos(fi);
	}

	Parabola Diocle::parable() const {
		if (this->assimpt == 0.0) {
			throw std::exception("Parabola for this source data isn't exist!");
		}
		Parabola Equasion;
		Equasion.a = (-1.0)*0.25* this->assimpt;
		Equasion.b = this->center.y / (2 * this->assimpt);
		Equasion.c = (((this->center.y) * (this->center.y)) / 4 * this->assimpt) + this->center.x;

		return Equasion;
	}

	Diocle::Diocle(double asmp) :center(0, 0)
	{ this->assimpt = asmp; }
	Diocle::Diocle(const Point& p0, double asmp) :center(p0)
	{ this->assimpt  = asmp; }
	Diocle::Diocle(double x0, double y0, double asmpt) :center(x0, y0)
	{ this->assimpt = asmpt;}
}