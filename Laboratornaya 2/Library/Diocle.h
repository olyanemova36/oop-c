#pragma once
#include <stdlib.h>
#include <crtdbg.h>
#include <math.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#ifdef _DEBUG
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the
// allocations to be of _CLIENT_BLOCK type
#else
#define DBG_NEW new
#endif

#define PI  3.14159

namespace Polar {

	struct Point {
		double x, y;
		Point(double x0 = 0, double y0 = 0) :x(x0), y(y0) {}
	};

	struct Parabola {
		double a;
		double b;
		double c;

		Parabola(double a0 = 0, double b0 = 0, double c0 = 0) : a(a0), b(b0), c(c0) {}
	};

	class Diocle {
	private:
		double assimpt;
		Point center;
	public:
		Diocle(double assimpt = 1); // singular state - just a point (0,0)
		Diocle(const Point& p0, double assimpt = 1);
		Diocle(double x0, double y0, double assimpt = 1);

		Diocle& setAssimpt(double A) { this->assimpt = A; return *this; }; //setter
		Diocle& setPoint(const Point& p0) { this->center = p0; return *this; }
		double qetAssimpt() const { return assimpt; }  // getter
		Point getCenter() const { return center; }
		//others methods
		Point ordinate(double x) const;
		double path(double fi) const;
		double assim() {
			return 2 * this->assimpt;
		}
		double square() {
			return 3 * PI * this->assimpt;
		}
		double volume() {
			return 10 * PI * PI * this->assimpt * this->assimpt * this->assimpt;
		}
		Parabola parable() const;
	};
}
